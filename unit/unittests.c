/**
 * @file tests.c
 *
 * Launch unit tests
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <malloc.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "../fileWalker.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(__WIN32__) \
	|| defined(WIN64) || defined(_WIN64) || defined(__WIN64) || defined(__WIN64__) \
	|| defined(WINNT) || defined(__WINNT) || defined(__WINNT__)
	#define IS_WIN
#endif

char* tmpDir;

char* directories[] = {
	"hgkf",
};
char* files[] = {
	"abc.d",
	"hgkf/qffh"
	"abf.g",
};
size_t directoriesLen = sizeof(directories) / sizeof(char*);
size_t filesLen = sizeof(files) / sizeof(char*);


/**
 * Translate a char to a char
 */
void strtr(char* str, char from, char to) {
	size_t i,
	       c;
	for (i = 0, c = strlen(str) ; i < c ; ++i) {
		if (from == str[i]) {
			str[i] = to;
		}
	}
}


void tests_verifyDirectories() {
	size_t foundDirectoriesLen = 0;
	char** foundDirectories = calloc(foundDirectoriesLen + 1, sizeof(char*));
	if (0 != errno) {
		perror("calloc");
		return;
	}

	void directoryProcessing(const struct dirent* file, const char* root) {
		size_t filepathLen = strlen(root) + strlen(file->d_name) + 2;
		char* filepath;

		filepath = (char*) calloc(filepathLen, sizeof(char));
		if (0 != errno) {
			perror("calloc");
			return;
		}

		snprintf(
			filepath,
			filepathLen * sizeof(char),
			"%s%s%s",
			root,
			PATH_SEPARATOR,
			file->d_name
		);
		if (0 != errno) {
			perror("snprintf");
			return;
		}

		foundDirectories[foundDirectoriesLen] =	filepath;

		++foundDirectoriesLen;

		foundDirectories = (char**) realloc(
			foundDirectories,
			(foundDirectoriesLen + 1) * sizeof(char *)
		);
		if (0 != errno) {
			perror("realloc");
			return;
		}
	}
	walkDir(tmpDir, directoryProcessing, WALK_PROC_IGN, WALK_PROC_IGN);

	{ // Verify result
		unsigned int i;
		char *filepath;
		size_t filepathLen;

		CU_ASSERT_EQUAL_FATAL(foundDirectoriesLen, directoriesLen);

		for (i = 0; i < directoriesLen ; ++i) {
			filepathLen = strlen(tmpDir) + strlen(directories[i]) + 2;
			filepath = (char*) calloc(filepathLen, sizeof(char));
			if (0 != errno) {
				perror("calloc");
				return;
			}

			snprintf(
				filepath,
				filepathLen,
				"%s%s%s",
				tmpDir,
				PATH_SEPARATOR,
				directories[i]
			);
			if (0 != errno) {
				perror("snprintf");
				return;
			}

			strtr(filepath, '/', PATH_SEPARATOR[0]);

			CU_ASSERT_STRING_EQUAL(foundDirectories[i], filepath);

			free(filepath);
			if (0 != errno) {
				perror("free");
				return;
			}

			free(foundDirectories[i]);
			foundDirectories[i] = NULL;
			if (0 != errno) {
				perror("free");
				return;
			}
		}

		free(foundDirectories);
		foundDirectories = NULL;
		if (0 != errno) {
			perror("free");
			return;
		}
	}
}


void tests_verifyFiles() {
	size_t foundFilesLen = 0;
	char** foundFiles = calloc(foundFilesLen + 1, sizeof(char*));
	if (0 != errno) {
		perror("calloc");
		return;
	}

	void fileProcessing(const struct dirent* file, const char* root) {
		size_t filepathLen = strlen(root) + strlen(file->d_name) + 2;
		char* filepath;

		filepath = (char*) calloc(filepathLen, sizeof(char));
		if (0 != errno) {
			perror("calloc");
			return;
		}

		snprintf(
			filepath,
			filepathLen * sizeof(char),
			"%s%s%s",
			root,
			PATH_SEPARATOR,
			file->d_name
		);
		if (0 != errno) {
			perror("snprintf");
			return;
		}

		foundFiles[foundFilesLen] =	filepath;

		++foundFilesLen;

		foundFiles = (char**) realloc(
			foundFiles,
			(foundFilesLen + 1) * sizeof(char *)
		);
		if (0 != errno) {
			perror("realloc");
			return;
		}
	}
	walkDir(tmpDir, WALK_PROC_IGN, fileProcessing, WALK_PROC_IGN);

	{ // Verify result
		unsigned int i;
		char *filepath;
		size_t filepathLen;

		CU_ASSERT_EQUAL_FATAL(foundFilesLen, filesLen);

		for (i = 0; i < filesLen ; ++i) {
			filepathLen = strlen(tmpDir) + strlen(files[i]) + 2;
			filepath = (char*) calloc(filepathLen, sizeof(char));
			if (0 != errno) {
				perror("calloc");
				return;
			}

			snprintf(
				filepath,
				filepathLen,
				"%s%s%s",
				tmpDir,
				PATH_SEPARATOR,
				files[i]
			);
			if (0 != errno) {
				perror("snprintf");
				return;
			}

			strtr(filepath, '/', PATH_SEPARATOR[0]);

			CU_ASSERT_STRING_EQUAL(foundFiles[i], filepath);

			free(filepath);
			if (0 != errno) {
				perror("free");
				return;
			}

			free(foundFiles[i]);
			foundFiles[i] = NULL;
			if (0 != errno) {
				perror("free");
				return;
			}
		}

		free(foundFiles);
		foundFiles = NULL;
		if (0 != errno) {
			perror("free");
			return;
		}
	}
}


/**
 * Init the test suite
 */
int tests_createFileTree() {
	FILE* f;
	unsigned int i;
	char *filepath;
	size_t filepathLen;

	errno = 0;

	tmpDir = tempnam(NULL, NULL);
	if (0 != errno) {
		perror("tempnam");
		return EXIT_FAILURE;
	}
	printf("tmpDir = `%s`\n", tmpDir);

	#ifdef IS_WIN
		mkdir(tmpDir);
	#else
		mkdir(tmpDir, S_IRWXU);
	#endif
	if (0 != errno) {
		perror("mkdir");
		return EXIT_FAILURE;
	}

	for (i = 0 ; i < directoriesLen ; ++i) {
		filepathLen = strlen(tmpDir) + strlen(directories[i]) + 2;
		filepath = (char*) calloc(filepathLen, sizeof(char));
		if (0 != errno) {
			perror("calloc");
			return EXIT_FAILURE;
		}

		snprintf(
			filepath,
			filepathLen,
			"%s%s%s",
			tmpDir,
			PATH_SEPARATOR,
			directories[i]
		);
		if (0 != errno) {
			perror("snprintf");
			return EXIT_FAILURE;
		}


		#ifdef IS_WIN
			mkdir(filepath);
		#else
			mkdir(filepath, S_IRWXU);
		#endif
		if (0 != errno) {
			perror("mkdir");
			return EXIT_FAILURE;
		}

		free(filepath);
		if (0 != errno) {
			perror("free");
			return EXIT_FAILURE;
		}
	}

	for (i = 0 ; i < filesLen ; ++i) {
		filepathLen = strlen(tmpDir) + strlen(files[i]) + 2;
		filepath = (char*) calloc(filepathLen, sizeof(char));
		if (0 != errno) {
			perror("calloc");
			return EXIT_FAILURE;
		}

		snprintf(
			filepath,
			filepathLen,
			"%s%s%s",
			tmpDir,
			PATH_SEPARATOR,
			files[i]
		);
		if (0 != errno) {
			perror("snprintf");
			return EXIT_FAILURE;
		}

		f = fopen(filepath, "w");
		if (0 != errno) {
			perror("fopen");
			return EXIT_FAILURE;
		}
		fclose(f);
		if (0 != errno) {
			perror("fclose");
			return EXIT_FAILURE;
		}

		free(filepath);
		if (0 != errno) {
			perror("free");
			return EXIT_FAILURE;
		}
	}

	return 0;
}


/**
 * Clean the test suite
 */
int tests_cleanSuite() {
	return 0;
}


int main() {
	unsigned int ret;
	CU_pSuite pSuite;

	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();


	// Init suite
	//////////////////////////////////////////////////
	pSuite = CU_add_suite(
		"fileWalker",
		tests_createFileTree,
		tests_cleanSuite
	);

	if (NULL == pSuite) {
		goto quit;
	}


	// Add tests
	//////////////////////////////////////////////////
	if (NULL == CU_add_test(pSuite, "Verify directories of the file tree", tests_verifyDirectories)) {
		goto quit;
	}

	if (NULL == CU_add_test(pSuite, "Verify files of the file tree", tests_verifyFiles)) {
		goto quit;
	}


	int cpt = 0;
	do {
	// Execute tests showing them on the console
	//////////////////////////////////////////////////
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	printf("\n");
	CU_basic_show_failures(CU_get_failure_list());
	printf("\n");
	++cpt;
	cpt=50;
	} while (cpt<50);


	// Clean and close
	//////////////////////////////////////////////////
quit:
	ret = CU_get_error();
	if (CUE_SUCCESS == ret) {
		ret = CU_get_number_of_tests_failed();
	}

	CU_cleanup_registry();
	return ret;
}
