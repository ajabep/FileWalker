/**
 * @file example1.c
 *
 * Basic usage of fileWalker, with usage of ignore function
 */

#include <stdio.h>
#include <string.h>

#include "../fileWalker.h"


void myFileProcessing(const struct dirent* file, const char* root) {
	printf("FileName = %s%s%s\n", root, PATH_SEPARATOR, file->d_name);
}

int main(int argc, char** argv) {
	if (2 != argc
			|| 0 == strcmp(argv[1], "-h")
			|| 0 == strcmp(argv[1], "-help")
			|| 0 == strcmp(argv[1], "--help")
	) {
		printf("Usage: %s <path to scan>\n", argv[0]);
		return 0;
	}

	walkDir(argv[1], WALK_PROC_IGN, myFileProcessing, WALK_PROC_IGN);
	return 0;
}
