/**
 * @file example1.c
 *
 * Basic usage of fileWalker
 */

#include <stdio.h>
#include <string.h>

#include "../fileWalker.h"


void myDirProcessing(const struct dirent* file, const char* root) {
	printf("DirName = %s%s%s\n", root, PATH_SEPARATOR, file->d_name);
}

void myFileProcessing(const struct dirent* file, const char* root) {
	printf("FileName = %s%s%s\n", root, PATH_SEPARATOR, file->d_name);
}

void myOtherProcessing(const struct dirent* file, const char* root) {
	printf("OtherName = %s%s%s\n", root, PATH_SEPARATOR, file->d_name);
}

int main(int argc, char** argv) {
	if (2 != argc
			|| 0 == strcmp(argv[1], "-h")
			|| 0 == strcmp(argv[1], "-help")
			|| 0 == strcmp(argv[1], "--help")
	) {
		printf("Usage: %s <path to scan>\n", argv[0]);
		return 0;
	}

	walkDir(argv[1], myDirProcessing, myFileProcessing, myOtherProcessing);
	return 0;
}
