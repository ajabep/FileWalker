
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "fileWalker.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(__WIN32__) \
	|| defined(WIN64) || defined(_WIN64) || defined(__WIN64) || defined(__WIN64__) \
	|| defined(WINNT) || defined(__WINNT) || defined(__WINNT__)
	#define IS_WIN
	#include <windows.h>
#endif



void walkDir(
	const char* path,
	void (*const directoryProcessing)(const struct dirent* file, const char* root),
	void (*const fileProcessing)(const struct dirent* file, const char* root),
	void (*const otherProcessing)(const struct dirent* file, const char* root)
) {
	DIR* cwd;
	struct dirent* file;
	char* dirPath;
	size_t dirPathLen;
	#ifdef IS_WIN
		DWORD attribute;
	#endif

	errno = 0;
	cwd = opendir(path);
	if (0 != errno) {
		perror("opendir");
		return;
	}

	while (NULL != (file = readdir(cwd)) && 0 == errno) {
		if (0 == strcmp(file->d_name, ".") || 0 == strcmp(file->d_name, "..")) {
			continue;
		}

		dirPathLen = strlen(path) + strlen(file->d_name) + 2;
		dirPath = (char*) calloc(dirPathLen, sizeof(char));
		if (0 != errno) {
			perror("calloc");
			goto walkDir_close;
		}
		snprintf(
			dirPath,
			dirPathLen,
			"%s%s%s",
			path,
			PATH_SEPARATOR,
			file->d_name
		);

		#ifdef IS_WIN
			attribute = GetFileAttributes(dirPath);
			if (attribute & FILE_ATTRIBUTE_DIRECTORY) {
		#else
			switch (file->d_type) {
				case DT_DIR:
		#endif
				directoryProcessing(file, path);

				walkDir(
					dirPath,
					directoryProcessing,
					fileProcessing,
					otherProcessing
				);
		#ifdef IS_WIN
			}
			else {
		#else
					break;
				case DT_REG:
		#endif
				fileProcessing(file, path);
		#ifdef IS_WIN
			}
		#else
					break;
				default:
					otherProcessing(file, path);
			}
		#endif

		errno = 0;
		free(dirPath);
		if (0 != errno) {
			perror("free");
			goto walkDir_close;
		}
		dirPath = NULL;
	}

	if (0 != errno) {
		perror("readdir");
		goto walkDir_close;
	}

walkDir_close:
	closedir(cwd);
	if (0 != errno) {
		perror("closedir");
		goto walkDir_close;
	}
}


void WALK_PROC_IGN(const struct dirent* file, const char* root) {
}

