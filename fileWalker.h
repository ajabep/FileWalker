#ifndef __FILE_WALKER_H__
#define __FILE_WALKER_H__

#include <sys/types.h>
#include <dirent.h>


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(__WIN32__) \
	|| defined(WIN64) || defined(_WIN64) || defined(__WIN64) || defined(__WIN64__) \
	|| defined(WINNT) || defined(__WINNT) || defined(__WINNT__)
	#define PATH_SEPARATOR "\\"
#else
	#define PATH_SEPARATOR "/"
#endif

/**
 * Walk in a directory and execute functions according to the type of found files
 *
 * @param path					the path of the directory we walk
 * @param directoryProcessing	pointer to the funciton of proccessing for every directories
 * @param fileProcessing		pointer to the funciton of proccessing for every regular files
 * @param otherProcessing		pointer to the funciton of proccessing for others. Only on UNIX systems
 */
void walkDir(
	const char* path,
	void (*const directoryProcessing)(const struct dirent* file, const char* root),
	void (*const fileProcessing)(const struct dirent* file, const char* root),
	void (*const otherProcessing)(const struct dirent* file, const char* root)
);

/**
 * Function of processing which does nothing
 */
void WALK_PROC_IGN(const struct dirent* file, const char* root);

#endif // __FILE_WALKER_H__
